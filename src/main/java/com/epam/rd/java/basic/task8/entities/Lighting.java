package com.epam.rd.java.basic.task8.entities;

public enum Lighting {
    YES("yes"),
    NO("no");

    private final String required;

    Lighting(String required) {
        this.required = required;
    }

    public String getRequired() {
        return required;
    }

    @Override
    public String toString() {
        return required;
    }

    public static Lighting determineLighting(String text){
        switch (text){
            case "yes":
                return Lighting.YES;
            case "no":
                return Lighting.NO;
            default:
                return null;
        }
    }
}
