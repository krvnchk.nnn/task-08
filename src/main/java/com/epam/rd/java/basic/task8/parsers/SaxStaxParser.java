package com.epam.rd.java.basic.task8.parsers;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.TransformerException;

import com.epam.rd.java.basic.task8.entities.Flowers;

public class SaxStaxParser {

 public static void writeXml(ArrayList<Flowers> flowerList, String filename) throws XMLStreamException, FileNotFoundException, TransformerException {
	XMLOutputFactory output = XMLOutputFactory.newInstance();

	XMLStreamWriter writer = output.createXMLStreamWriter(new FileOutputStream(filename));
	
	writer.writeStartDocument("utf-8", "1.0");
	
	writer.writeStartElement("flowers");
	writer.writeAttribute("xmlns", "http://www.nure.ua");
	writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
	writer.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
	
	for (int i = 0; i < flowerList.size(); i++) {

		writer.writeStartElement("flower");
		
		writer.writeStartElement("name");
		writer.writeCharacters(flowerList.get(i).getName());
		writer.writeEndElement();
		
		writer.writeStartElement("soil");
		writer.writeCharacters(flowerList.get(i).getSoil().getType());
		writer.writeEndElement();
		
		writer.writeStartElement("origin");
		writer.writeCharacters(flowerList.get(i).getOrigin());
		writer.writeEndElement();
		
		writer.writeStartElement("visualParameters");
		
		writer.writeStartElement("stemColour");
		writer.writeCharacters(flowerList.get(i).getVisualParameters().getStemColour());
		writer.writeEndElement();
		
		writer.writeStartElement("leafColour");
		writer.writeCharacters(flowerList.get(i).getVisualParameters().getLeafColour());
		writer.writeEndElement();
		
		writer.writeStartElement("aveLenFlower");
		writer.writeAttribute("measure", "cm");
		writer.writeCharacters(String.valueOf(flowerList.get(i).getVisualParameters().getAveLenFlower()));
		writer.writeEndElement();
		
		writer.writeEndElement();
		
		writer.writeStartElement("growingTips");
		
		writer.writeStartElement("tempreture");
		writer.writeAttribute("measure", "celcius");
		writer.writeCharacters(String.valueOf(flowerList.get(i).getGrowingTips().getTemperature()));
		writer.writeEndElement();
		
		writer.writeEmptyElement("lighting");
		writer.writeAttribute("lightRequiring", flowerList.get(i).getGrowingTips().getLighting().getRequired());
		
		writer.writeStartElement("watering");
		writer.writeAttribute("measure", "mlPerWeek");
		writer.writeCharacters(String.valueOf(flowerList.get(i).getGrowingTips().getWatering()));
		writer.writeEndElement();
		
		writer.writeEndElement();
		
		writer.writeStartElement("multiplying");
		writer.writeCharacters(flowerList.get(i).getMultiplying().getType());
		writer.writeEndElement();
		
		writer.writeEndElement();
	}
	
	writer.writeEndElement();
	
	writer.writeEndDocument();
	
	writer.flush();
	writer.close();

	}
}
