package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.entities.Flowers;
import com.epam.rd.java.basic.task8.parsers.DomParser;
import com.epam.rd.java.basic.task8.parsers.SaxStaxParser;
import com.epam.rd.java.basic.task8.controller.*;
import org.xml.sax.XMLReader;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import java.util.ArrayList;
import java.util.Comparator;

public class Main {

	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}

		String xmlFileName = args[0];
		ArrayList<Flowers> flowersList;

		DOMController domController = new DOMController(xmlFileName);
		flowersList = domController.DOM();

		flowersList.sort(new Comparator<Flowers>() {
			@Override
			public int compare(Flowers f1, Flowers f2) {
				return f1.getGrowingTips().getTemperature() - f2.getGrowingTips().getTemperature();
			}
		});

		String outputXmlFile = "output.dom.xml";
		DomParser.writeXml(flowersList, outputXmlFile);




		SAXParserFactory spf = SAXParserFactory.newInstance();
		SAXParser saxParser = spf.newSAXParser();
		XMLReader xmlReader = saxParser.getXMLReader();
		SAXController saxController = new SAXController(xmlFileName);
		xmlReader.setContentHandler(saxController);
		xmlReader.parse("input.xml");
		flowersList = saxController.getResult();


		flowersList.sort(new Comparator<Flowers>() {
			@Override
			public int compare(Flowers f1, Flowers f2) {
				return f1.getVisualParameters().getAveLenFlower() - f2.getVisualParameters().getAveLenFlower();
			}
		});

		outputXmlFile = "output.sax.xml";
		SaxStaxParser.writeXml(flowersList, outputXmlFile);




		STAXController staxController = new STAXController(xmlFileName);
		flowersList = staxController.parse();


		flowersList.sort(new Comparator<Flowers>() {
			@Override
			public int compare(Flowers f1, Flowers f2) {
				return f1.getName().compareTo(f2.getName());
			}
		});

		outputXmlFile = "output.stax.xml";
		SaxStaxParser.writeXml(flowersList, outputXmlFile);

	}

}
