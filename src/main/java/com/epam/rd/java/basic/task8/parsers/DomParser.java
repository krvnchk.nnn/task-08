package com.epam.rd.java.basic.task8.parsers;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.epam.rd.java.basic.task8.entities.Flowers;

public class DomParser {
	public static void writeXml(ArrayList<Flowers> flowerList, String fileName) throws ParserConfigurationException, TransformerException, FileNotFoundException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("flowers");
        rootElement.setAttribute("xmlns", "http://www.nure.ua");
        rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        rootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");

        doc.appendChild(rootElement);

        for (int i = 0; i < flowerList.size(); i++) {


            Element flower = doc.createElement("flower");
            rootElement.appendChild(flower);

            Element name = doc.createElement("name");
            name.setTextContent(flowerList.get(i).getName());
            flower.appendChild(name);

            Element soil = doc.createElement("soil");
            soil.setTextContent(flowerList.get(i).getSoil().getType());
            flower.appendChild(soil);

            Element origin = doc.createElement("origin");
            origin.setTextContent(flowerList.get(i).getOrigin());
            flower.appendChild(origin);

            Element visualParameters = doc.createElement("visualParameters");
            flower.appendChild(visualParameters);

            Element stemColour = doc.createElement("stemColour");
            stemColour.setTextContent(flowerList.get(i).getVisualParameters().getStemColour());
            visualParameters.appendChild(stemColour);

            Element leafColour = doc.createElement("leafColour");
            leafColour.setTextContent(flowerList.get(i).getVisualParameters().getLeafColour());
            visualParameters.appendChild(leafColour);

            Element aveLenFlower = doc.createElement("aveLenFlower");
            aveLenFlower.setAttribute("measure", "cm");
            aveLenFlower.setTextContent(String.valueOf(flowerList.get(i).getVisualParameters().getAveLenFlower()));
            visualParameters.appendChild(aveLenFlower);

            Element growingTips = doc.createElement("growingTips");
            flower.appendChild(growingTips);

            Element tempreture = doc.createElement("tempreture");
            tempreture.setAttribute("measure", "celcius");
            tempreture.setTextContent(String.valueOf(flowerList.get(i).getGrowingTips().getTemperature()));
            growingTips.appendChild(tempreture);

            Element lighting = doc.createElement("lighting");
            lighting.setAttribute("lightRequiring", flowerList.get(i).getGrowingTips().getLighting().getRequired());
            growingTips.appendChild(lighting);

            Element watering = doc.createElement("watering");
            watering.setAttribute("measure", "mlPerWeek");
            watering.setTextContent(String.valueOf(flowerList.get(i).getGrowingTips().getWatering()));
            growingTips.appendChild(watering);

            Element multiplying = doc.createElement("multiplying");
            multiplying.setTextContent(flowerList.get(i).getMultiplying().getType());
            flower.appendChild(multiplying);

        }
        
        prettyOutput(doc, new FileOutputStream(fileName));
    }

    public static void prettyOutput(Document doc, OutputStream output) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(output);

        transformer.transform(source, result);
    }
}
