package com.epam.rd.java.basic.task8.controller;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.namespace.QName;

import org.xml.sax.helpers.DefaultHandler;

import com.epam.rd.java.basic.task8.entities.*;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private static String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public ArrayList <Flowers> parse() {
        ArrayList<Flowers> flowersList = new ArrayList<Flowers>();
        Flowers currentFlower = null;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
            while (reader.hasNext()) {
                XMLEvent nextEvent = reader.nextEvent();
                if (nextEvent.isStartElement()) {
                    StartElement startElement = nextEvent.asStartElement();
                    switch (startElement.getName().getLocalPart()) {
                    case flowersConstants.FLOWER_TAG:
                        currentFlower = new Flowers();
                        break;
                    case flowersConstants.NAME_TAG:
                        nextEvent = reader.nextEvent();
                        currentFlower.setName(nextEvent.asCharacters().getData());
                        break;
                    case flowersConstants.SOIL_TAG:
                        nextEvent = reader.nextEvent();
                        currentFlower.setSoil(Soil.determineSoil(nextEvent.asCharacters().getData()));
                        break;
                    case flowersConstants.ORIGIN_TAG:
                        nextEvent = reader.nextEvent();
                        currentFlower.setOrigin(nextEvent.asCharacters().getData());
                        break;
                    case flowersConstants.VISUAL_PARAMETERS_TAG:
        				VisualParameters currentVisualParameters = new VisualParameters();
        				currentFlower.setVisualParameters(currentVisualParameters);
        				break;
        			case flowersConstants.GROWING_TIPS_TAG:
        				GrowingTips currentGrowingTips = new GrowingTips();
        				currentFlower.setGrowingTips(currentGrowingTips);
        				break;
        			case flowersConstants.STEM_COLOUR_TAG:
        				nextEvent = reader.nextEvent();
        				currentFlower.getVisualParameters().setStemColour(nextEvent.asCharacters().getData());
        				break;
        			case flowersConstants.LEAF_COLOUR_TAG:
        				nextEvent = reader.nextEvent();
        				currentFlower.getVisualParameters().setLeafColour(nextEvent.asCharacters().getData());
        				break;
        			case flowersConstants.AVE_LEN_FLOWER_TAG:
        				nextEvent = reader.nextEvent();
        				currentFlower.getVisualParameters().setAveLenFlower(Integer.parseInt(nextEvent.asCharacters().getData()));
        				break;
        			case flowersConstants.TEMPERATURE_TAG:
        				nextEvent = reader.nextEvent();
        				currentFlower.getGrowingTips().setTemperature(Integer.parseInt(nextEvent.asCharacters().getData()));
        				break;
        			case flowersConstants.LIGHTING_TAG:
        				 nextEvent = reader.nextEvent();
	                     Attribute lighting = startElement.getAttributeByName(new QName(flowersConstants.LIGHT_REQUIRING_TAG));
	                     if (lighting != null) {
	                      currentFlower.getGrowingTips().setLighting(Lighting.determineLighting(lighting.getValue()));
	                     }
        				break;
        			case flowersConstants.WATERING_TAG:
        				nextEvent = reader.nextEvent();
        				currentFlower.getGrowingTips().setWatering(Integer.parseInt(nextEvent.asCharacters().getData()));
        				break;
        			case flowersConstants.MULTIPLYING_TAG:
        				nextEvent = reader.nextEvent();
        				currentFlower.setMultiplying(Multiplying.determineMultiplying(nextEvent.asCharacters().getData()));
        				break;
                    }
                    
                }
                if (nextEvent.isEndElement()) {
                    EndElement endElement = nextEvent.asEndElement();
                    if (endElement.getName()
                        .getLocalPart()
                        .equals(flowersConstants.FLOWER_TAG)) {
                    	flowersList.add(currentFlower);
                    }
                }
            }
        } catch (XMLStreamException xse) {
            System.out.println("XMLStreamException");
            xse.printStackTrace();
        } catch (FileNotFoundException fnfe) {
            System.out.println("FileNotFoundException");
            fnfe.printStackTrace();
        }
        
        System.out.println("This is STAX!");
		flowersConstants.outputFlowersList(flowersList);
        return flowersList;
    }

}