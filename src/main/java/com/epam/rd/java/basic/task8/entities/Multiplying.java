package com.epam.rd.java.basic.task8.entities;


public enum Multiplying {
    SEEDS("семена"),
    CUTTINGS("черенки"),
    LEAVES("листья");

    private final String type;

    Multiplying(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return type;
    }

    public static Multiplying determineMultiplying(String text){
        switch (text){
            case "семена":
                return Multiplying.SEEDS;
            case "черенки":
                return Multiplying.CUTTINGS;
            case "листья":
                return Multiplying.LEAVES;
            default:
                return null;
        }
    }
}
