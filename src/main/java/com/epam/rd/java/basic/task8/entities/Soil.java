package com.epam.rd.java.basic.task8.entities;


public enum Soil {
    SOD_PODZOLIC("дерново-подзолистая"),
    UNPAVED("грунтовая"),
    PODZOLIC("подзолистая");

    private final String type;

    Soil(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }


    @Override
    public String toString() {
        return type;
    }

    public static Soil determineSoil(String text){
        switch (text){
            case "дерново-подзолистая":
                return Soil.SOD_PODZOLIC;
            case "грунтовая":
                return Soil.UNPAVED;
            case "подзолистая":
                return Soil.PODZOLIC;
            default:
                return null;
        }
    }

}
