
package com.epam.rd.java.basic.task8.controller;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.epam.rd.java.basic.task8.entities.*;


//import static com.epam.rd.java.basic.task8.entity.flowerConstants.*;

/**
 * Controller for DOM parser.
 */
public class DOMController {

  private String xmlFileName;

  public DOMController(String xmlFileName) {
    this.xmlFileName = xmlFileName;
  }

  public ArrayList<Flowers> DOM() {
    File file = new File(xmlFileName);
    Flowers currentFlower = new Flowers();
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    Document doc = null;
    try {
      doc = dbf.newDocumentBuilder().parse(file);
    } catch (SAXException | IOException | ParserConfigurationException e) {
      e.printStackTrace();
    }

    Node rootNode = doc.getFirstChild();
    NodeList rootChilds = rootNode.getChildNodes();

    ArrayList<Flowers> list = new ArrayList<>();

    for (int i = 0; i < rootChilds.getLength(); i++) {
      if (rootChilds.item(i).getNodeType() != Node.ELEMENT_NODE) {
        continue;
      }

      if (!rootChilds.item(i).getNodeName().equals("flower")) {
        continue;
      }

      String name = null;
      Soil soil = null;
      String origin = null;
      VisualParameters visualParameters = null;
      GrowingTips growingTips = null;
      Multiplying multiplying = null;

      NodeList elementChilds = rootChilds.item(i).getChildNodes();
      for (int j = 0; j < elementChilds.getLength(); j++) {

        if (elementChilds.item(j).getNodeType() != Node.ELEMENT_NODE) {
          continue;
        }

        switch (elementChilds.item(j).getNodeName()) {
          case flowersConstants.NAME_TAG:
            name = elementChilds.item(j).getTextContent();
            break;

          case flowersConstants.SOIL_TAG:
            soil = Soil.determineSoil(elementChilds.item(j).getTextContent());
            break;

          case flowersConstants.ORIGIN_TAG:
            origin = elementChilds.item(j).getTextContent();
            break;

          case flowersConstants.VISUAL_PARAMETERS_TAG:
            String stemColour = null;
            String leafColour = null;
            int aveLenFlower = 0;

            NodeList visualParametersNode = elementChilds.item(j).getChildNodes();

            for (int k = 0; k < visualParametersNode.getLength(); k++) {

              if (elementChilds.item(k).getNodeType() != Node.ELEMENT_NODE) {
                continue;
              }

              switch (visualParametersNode.item(k).getNodeName()) {
                case flowersConstants.STEM_COLOUR_TAG:
                  stemColour = visualParametersNode.item(k).getTextContent();
                  break;
                case flowersConstants.LEAF_COLOUR_TAG:
                  leafColour = visualParametersNode.item(k).getTextContent();
                  break;
                case flowersConstants.AVE_LEN_FLOWER_TAG:
                  aveLenFlower = Integer.parseInt(visualParametersNode.item(k).getTextContent());
                  break;
              }
            }
            visualParameters = new VisualParameters(stemColour, leafColour, aveLenFlower);

          case flowersConstants.GROWING_TIPS_TAG:
            int temperature = 0;
            Lighting lighting = null;
            int watering = 0;

            NodeList growingTipsNode = elementChilds.item(j).getChildNodes();

            for (int k = 0; k < growingTipsNode.getLength(); k++) {
              if (elementChilds.item(k).getNodeType() != Node.ELEMENT_NODE) {
                continue;
              }

              switch (growingTipsNode.item(k).getNodeName()) {
                case flowersConstants.TEMPERATURE_TAG:
                  temperature = Integer.parseInt(growingTipsNode.item(k).getTextContent());
                  break;
                case flowersConstants.LIGHTING_TAG:
                	NamedNodeMap attributes = growingTipsNode.item(k).getAttributes();
                	Node theAttribute = attributes.item(0);
                	lighting = Lighting.determineLighting(theAttribute.getNodeValue());
                  break;
                case flowersConstants.WATERING_TAG:
                  watering = Integer.parseInt(growingTipsNode.item(k).getTextContent());
              }
            }
            growingTips = new GrowingTips(temperature, lighting, watering);

          case flowersConstants.MULTIPLYING_TAG:
            multiplying = Multiplying.determineMultiplying(elementChilds.item(j).getTextContent());
        }
      }
      list.add(new Flowers(name, soil, origin, visualParameters, growingTips, multiplying));
    }
    
    
    System.out.println("This is DOM!");
    flowersConstants.outputFlowersList(list);
    return list;
  }


}