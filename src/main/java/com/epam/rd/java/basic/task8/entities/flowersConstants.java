package com.epam.rd.java.basic.task8.entities;

import java.util.ArrayList;

public class flowersConstants {
	public static final String FLOWERS_TAG = "flowers";
	public static final String FLOWER_TAG = "flower";
	public static final String NAME_TAG = "name";
	public static final String SOIL_TAG = "soil";
	public static final String ORIGIN_TAG = "origin";
	public static final String VISUAL_PARAMETERS_TAG = "visualParameters";
	public static final String STEM_COLOUR_TAG = "stemColour";
	public static final String LEAF_COLOUR_TAG = "leafColour";
	public static final String AVE_LEN_FLOWER_TAG = "aveLenFlower";
	public static final String GROWING_TIPS_TAG = "growingTips";
	public static final String TEMPERATURE_TAG = "tempreture";
	public static final String LIGHTING_TAG = "lighting";
	public static final String LIGHT_REQUIRING_TAG = "lightRequiring";
	public static final String WATERING_TAG = "watering";
	public static final String MULTIPLYING_TAG = "multiplying";
	
	public static void outputFlowersList(ArrayList<Flowers> flowersList) {
		for(Flowers f: flowersList){
			System.out.println(f);
		}
		System.out.println("\n\n");
	}

}
