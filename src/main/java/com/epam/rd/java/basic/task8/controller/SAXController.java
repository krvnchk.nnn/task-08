package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entities.*;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private ArrayList<Flowers> flowersList;
	private Flowers currentFlower;
	private String currentElement;

	public SAXController(String xmlFileName) {
	}

	public ArrayList<Flowers> getResult(){
		return flowersList;
	}


	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		currentElement = qName;
		switch(currentElement){
			case flowersConstants.FLOWERS_TAG:
				flowersList = new ArrayList<>();
				break;
			case flowersConstants.FLOWER_TAG:
				currentFlower = new Flowers();
				break;

			case flowersConstants.VISUAL_PARAMETERS_TAG:
				VisualParameters currentVisualParameters = new VisualParameters();
				currentFlower.setVisualParameters(currentVisualParameters);
				break;

			case flowersConstants.GROWING_TIPS_TAG:
				GrowingTips currentGrowingTips = new GrowingTips();
				currentFlower.setGrowingTips(currentGrowingTips);
				break;
			case flowersConstants.LIGHTING_TAG:
				currentFlower.getGrowingTips().setLighting(Lighting.determineLighting(attributes.getValue(flowersConstants.LIGHT_REQUIRING_TAG)));
				break;
			
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		String text = new String(ch, start, length);

		if (text.contains("<") || currentElement == null){
			return;
		}

		switch(currentElement){
			case flowersConstants.NAME_TAG:
				currentFlower.setName(text);
				break;
			case flowersConstants.SOIL_TAG:
				currentFlower.setSoil(Soil.determineSoil(text));
				break;
			case flowersConstants.ORIGIN_TAG:
				currentFlower.setOrigin(text);
				break;
			case flowersConstants.STEM_COLOUR_TAG:
				currentFlower.getVisualParameters().setStemColour(text);
				break;
			case flowersConstants.LEAF_COLOUR_TAG:
				currentFlower.getVisualParameters().setLeafColour(text);
				break;
			case flowersConstants.AVE_LEN_FLOWER_TAG:
				currentFlower.getVisualParameters().setAveLenFlower(Integer.parseInt(text));
				break;
			case flowersConstants.TEMPERATURE_TAG:
				currentFlower.getGrowingTips().setTemperature(Integer.parseInt(text));
				break;
			case flowersConstants.WATERING_TAG:
				currentFlower.getGrowingTips().setWatering(Integer.parseInt(text));
				break;
			case flowersConstants.MULTIPLYING_TAG:
				currentFlower.setMultiplying(Multiplying.determineMultiplying(text));
				break;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (flowersConstants.FLOWER_TAG.equals(qName)) {
			if (currentFlower != null) {
				flowersList.add(currentFlower);
				currentFlower = null;
			}
		} 
		currentElement = null;
	}

	@Override
	public void endDocument() throws SAXException {
		System.out.println("This is SAX!");
		flowersConstants.outputFlowersList(flowersList);
	}
	
	

}