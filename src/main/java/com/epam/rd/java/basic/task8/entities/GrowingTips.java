package com.epam.rd.java.basic.task8.entities;


public class GrowingTips {
    private int temperature;
    private Lighting lighting;
    private int watering;

    public GrowingTips(){}

    public GrowingTips(int temperature, Lighting lighting, int watering) {
        this.temperature = temperature;
        this.lighting = lighting;
        this.watering = watering;
    }

    public int getTemperature() {
        return temperature;
    }

    public Lighting getLighting() {
        return lighting;
    }

    public int getWatering() {
        return watering;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public void setLighting(Lighting lighting) {
        this.lighting = lighting;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "temperature=" + temperature +
                ", lighting=" + lighting +
                ", watering=" + watering +
                '}';
    }
}
